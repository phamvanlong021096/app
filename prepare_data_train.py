import os
import re
import docx2txt


def read_file_docx(path):
    text = docx2txt.process(path)
    text = text.lower()
    lines = text.split('\n')
    lines = [l.strip() + '\n' for l in lines if l != '']
    return lines


def prepare_skill_array(list_skill, list_word):
    is_skill = False
    skill = ''
    for word in list_word:
        if word == '<skill>':
            is_skill = True
        elif word == '</skill>':
            # skill = make_string_clean(skill)
            skill = skill.strip()
            if skill not in list_skill:
                list_skill.append(skill)
            is_skill = False
            skill = ''
        else:
            if is_skill:
                skill = skill + word + ' '
    list_skill = [sk for sk in list_skill if sk != '']
    return list_skill


def write_to_file(list, file_path):
    with open(file_path, 'w', encoding='utf-8') as f:
        for element in list:
            el = element + '\n'
            f.write(el)


def convert_B_I(list_word, begin_labels, end_labels, labels, file_path):
    label = '0'
    lb = ''
    is_begin_entity = False
    with open(file_path, 'a', encoding='utf-8') as f:
        for word in list_word:
            if word in begin_labels:
                lb = labels[word]
                label = 'B_' + lb
                is_begin_entity = True
            elif word in end_labels:
                lb = ''
                label = '0'
                is_begin_entity = False
            else:
                row = word + ' ' + label + '\n'
                f.write(row)
                if is_begin_entity:
                    is_begin_entity = False
                    label = 'I_' + lb
        f.write('\n')


def main():
    begin_labels = ['<university>', '<degree>', '<exp>', '<skill_language>']
    end_labels = ['</university>', '</degree>', '</exp>', '</skill_language>']
    labels = {'<university>': 'UNIVERSITY',
              '<degree>': 'DEGREE',
              '<exp>': 'EXP',
              '<skill_language>': 'SKILL_LANGUAGE'}

    resume_directory = 'D:/Win10/resume_file/DATA_TRAIN'  # path to resume directory
    file_resume_paths = [os.path.join(resume_directory, f)
                         for f in os.listdir(resume_directory)
                         if f.endswith('.docx')]
    file_rule_skill = os.getcwd() + '/config/rule_skill.txt'
    file_data_train = os.getcwd() + '/data/data_train.txt'

    # delete if exit
    if os.path.exists(file_rule_skill):
        os.remove(file_rule_skill)
    if os.path.exists(file_data_train):
        os.remove(file_data_train)

    list_skill = []

    # prepare data train and write to file_rule_skill and file_data_train
    for path in file_resume_paths:
        lines = read_file_docx(path)

        # prepare skill
        for l in lines:
            list_word = re.split('[.,\n\t\s]', l)
            list_word = [w for w in list_word if w != '']
            list_skill = prepare_skill_array(list_skill, list_word)
        write_to_file(list_skill, file_rule_skill)

        # convert sang dang B I
        for l in lines:
            list_word = re.split('[-_+:;.,*!\n\t\s]', l)
            list_word = [w for w in list_word if w != '']
            # print(list_word)
            convert_B_I(list_word, begin_labels, end_labels, labels, file_data_train)

    print("Prepare data train done!")


if __name__ == '__main__':
    main()
