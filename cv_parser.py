import os
import re
import docx2txt


def get_topic(file_name):
    path = os.getcwd() + '/config/' + file_name
    with open(path, 'r', encoding='utf-8') as f:
        content = f.readlines()
        topics = [line.strip() for line in content if line != '' and line != '\n']
    return topics


def get_topic_header(topics):
    headers = {}
    for topic in topics:
        path = os.getcwd() + '/config/' + topic + '.txt'
        with open(path, 'r', encoding='utf-8') as f:
            content = f.readlines()
            header = [line.strip() for line in content if line != '' and line != '\n']
            headers[topic] = header
    return headers


def read_resume_docx(path):
    text = docx2txt.process(path)
    lines = text.split('\n')
    lines = [l.strip() + '\n' for l in lines if l != '']
    content = ''
    for line in lines:
        content = content + line
    return content


def content_ok(text, tp_obj1, tp_obj2, topic_objs):
    for tp_obj in topic_objs:
        if tp_obj.get_topic() != tp_obj1.get_topic() and tp_obj.get_topic() != tp_obj2.get_topic():
            header_list = tp_obj.get_header()
            for header in header_list:
                regex = r"" + header + "\s*:?\n+"
                pattern = re.compile(regex, re.IGNORECASE | re.UNICODE)
                if pattern.search(text):
                    return False
    return True


def find_content(text, tp_obj1, tp_obj2, topic_objs):
    header_list1 = tp_obj1.get_header()
    header_list2 = tp_obj2.get_header()
    for header1 in header_list1:
        for header2 in header_list2:
            regex = r"" + header1 + "\s*:?\n+([.\s\S]*)\n.*" + header2 + "\s*:?\n+"
            pattern = re.compile(regex, re.IGNORECASE | re.UNICODE)
            match = pattern.search(text)
            if match:
                content = match.group(1)
                if content_ok(content, tp_obj1, tp_obj2, topic_objs):
                    # print("tim thay " + key1 + " " + key2)
                    return True, content
    return False, ""


def get_list_skill(file_name):
    path = os.getcwd() + '/config/rule_skill.txt'
    with open(path, 'r', encoding='utf-8') as f:
        content = f.readlines()
        list_skill = [line.strip() for line in content if line != '' and line != '\n']
    return list_skill


def get_topic_object(topic_name, topic_objs):
    for topic in topic_objs:
        if topic.get_topic() == topic_name:
            return topic


def execute_regex(text, regex):
    pattern = re.compile(regex, re.IGNORECASE | re.UNICODE)
    return pattern.search(text)


def find_name(text):
    regex = "tên\s*:?\s*(.+)"
    res = execute_regex(text, regex)
    if res:
        return res.group(1)
    return ""


def find_address(text):
    regex = "địa chỉ\s*:?\s*(.+)"
    res = execute_regex(text, regex)
    if res:
        return res.group(1)
    return ""


def find_birthday(text):
    regex = "([0-9]+[\/-][0-9]+[\/-][0-9]+)"
    res = execute_regex(text, regex)
    if res:
        return res.group(1)
    return ""


def find_email(text):
    regex = "([\w\S]+@[\w\.]+\.com)"
    res = execute_regex(text, regex)
    if res:
        return res.group(1)
    return ""


def find_phone(text):
    regex = "(\d{4}[-\.\s]??\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}" \
            "|\d{3}[-\.\s]??\d{4})"
    res = execute_regex(text, regex)
    if res:
        return res.group(1)
    return ""


def find_gender(text):
    regex = "giới tính\s*:?\s*(\w+)"
    res = execute_regex(text, regex)
    if res:
        return res.group(1)
    return ""


def find_university(text):
    regex = "(đại học\s*:?\s*.+|đh\s*:?\s*.+|dh\s*:?\s*.+)"
    res = execute_regex(text, regex)
    if res:
        return res.group(1)
    return ""


def find_degree(text):
    return ""


def find_skill(text, list_skill):
    skills = []
    for sk in list_skill:
        if sk in text:
            skills.append(sk)
    return skills


def find_position(text):
    return ""


def find_exp(text):
    return []


def find_skill_language(text):
    return []


def test():
    print("test")
